<?php 
	class Database{
		private static $dbName = 'crud';
		private static $host = 'localhost';
		private static $userName = 'shukhrat';
		private static $userPassword = 'qwerty123';

		private static $cont = null;

		public function __construct(){
			die('Init function is not allowed');
		}

		public static function connect(){
			if(null == self::$cont){
				try{
					self::$cont = new PDO('mysql:host='.self::$host.';'.'dbname='.self::$dbName, self::$userName, self::$userPassword);
				}
				catch(PDOException $e){
					die($e->getMessage());
				}
			}
			return self::$cont;
		}

		public static function disconnect(){
			self::$cont = null;
		}
}
?>